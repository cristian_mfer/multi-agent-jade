package isia.agents;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.io.FileReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class DatabaseAgent extends Agent {

	protected void setup() {
		try {
			String tipo;

			Object[] args = getArguments();

			// Parse JSON input file
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(new FileReader((String) args[0]));

			JSONObject jsonObject = (JSONObject) obj;

			// Obtenemos el tipo del agente de base de datos
			tipo = (String) jsonObject.get("tipo");

			// Registramos el agente en las p�ginas amarillas
			DFAgentDescription dfd = new DFAgentDescription();
			dfd.setName(getAID());
			ServiceDescription sd = new ServiceDescription();
			sd.setType(tipo);
			sd.setName(getLocalName());
			dfd.addServices(sd);
			DFService.register(this, dfd);

			addBehaviour(new DatabaseBehaviour());
			System.out.println("DATB --> Agente de base de datos " + getLocalName()
					+ " inicializado.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void takeDown() {
		try {
			DFService.deregister(this);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		System.out.println("DATB --> El agente " + getAID().getName()
				+ " ya no ofrece sus servicios.");
	}

	private class DatabaseBehaviour extends CyclicBehaviour {

		public void action() {
			MessageTemplate mt = MessageTemplate
					.MatchPerformative(ACLMessage.QUERY_REF);
			ACLMessage msg = myAgent.receive(mt);
			if (msg != null) {
				System.out
						.println("DATB --> Mensaje recibido: "
								+ msg.getContent());
				ACLMessage respuesta = msg.createReply();
				respuesta.setPerformative(ACLMessage.INFORM);
				respuesta.setContent("Datos del agente de conocimiento");
				send(respuesta);

			} else {
				block();
			}
		}

		public int onEnd() {
			myAgent.doDelete();
			return super.onEnd();
		}
	}
}
