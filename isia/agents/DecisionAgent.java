package isia.agents;

import isia.behaviours.BehaviourReceiverMsg;
import isia.onto.Habitacion;
import isia.onto.OntologiaEdificioInteligente;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.UngroundedException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.io.FileReader;
import java.util.Random;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class DecisionAgent extends Agent {
	private Ontology ontologia = OntologiaEdificioInteligente.getInstance();
	private Codec codec = new SLCodec();
	
	protected void setup() {
		try {
			String tipo, tipo_sensor, tipo_accion, tipo_conocimiento;

			Object[] args = getArguments();

			// Parse JSON input file
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(new FileReader((String) args[0]));

			JSONObject jsonObject = (JSONObject) obj;

			// Obtenemos el tipo de agentes sensores a observar
			tipo_sensor = (String) jsonObject.get("tipo_sensor");

			// Obtenemos el tipo del agente de decision
			tipo = (String) jsonObject.get("tipo");

			// Obtenemos el tipo del agente de accion
			tipo_accion = (String) jsonObject.get("tipo_accion");

			// Obtenemos el tipo del agente de base de datos
			tipo_conocimiento = (String) jsonObject.get("tipo_conocimiento");

			// Registramos el agente en las p�ginas amarillas
			DFAgentDescription dfd = new DFAgentDescription();
			dfd.setName(getAID());
			ServiceDescription sd = new ServiceDescription();
			sd.setType(tipo);
			sd.setName(getLocalName());
			dfd.addServices(sd);
			DFService.register(this, dfd);
			
			getContentManager().registerLanguage(codec);
			getContentManager().registerOntology(ontologia);

			addBehaviour(new DecisionBehaviour(tipo_sensor, tipo_accion,
					tipo_conocimiento));
			System.out.println(tipo_sensor);

			System.out.println("DEC --> Agente de decision " + getLocalName()
					+ " inicializado.");
		} catch (Exception er) {
			er.printStackTrace();
		}
	}

	protected void takeDown() {
		try {
			DFService.deregister(this);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		System.out.println("DEC --> El agente " + getAID().getName()
				+ " ya no ofrece sus servicios.");
	}

	private class DecisionBehaviour extends CyclicBehaviour {
		AID[] agentesSensores;
		String tipo_sensores, tipo_accion, tipo_conocimiento;
		Random rand = new Random();

		public DecisionBehaviour(String tipo_sensores, String tipo_accion,
				String tipo_conocimiento) {
			this.tipo_sensores = tipo_sensores;
			this.tipo_accion = tipo_accion;
			this.tipo_conocimiento = tipo_conocimiento;
		}

		public void onStart() {
			try {
				DFAgentDescription template = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType(tipo_sensores);
				template.addServices(sd);
				DFAgentDescription[] result = DFService.search(myAgent,
						template);

				agentesSensores = new AID[result.length];
				for (int i = 0; i < result.length; ++i) {
					System.out.println("DEC --> Encontrado un agente sensor "
							+ result[i].getName().getName());
					agentesSensores[i] = result[i].getName();

					ACLMessage msg = new ACLMessage(ACLMessage.SUBSCRIBE);
					msg.addReceiver(result[i].getName());
					msg.setSender(myAgent.getAID());
					msg.setLanguage("Spanish");
					msg.setContent("Subscripcion a eventos");
					myAgent.send(msg);
					System.out
							.println("DEC --> Agente de decision manda mensaje subscribe");
				}

			} catch (Exception er) {
				er.printStackTrace();
			}

		}

		public void action() {
			try {
				MessageTemplate mt = MessageTemplate
						.MatchPerformative(ACLMessage.INFORM);
				ACLMessage msg = myAgent.receive(mt);
				if (msg != null) { // Hemos recibido un mensaje de un agente
									// sensor
					System.out.println("DEC --> Mensage recibido: "
							+ msg.getContent());
					
					ContentElement ce = getContentManager().extractContent(msg);
					if (ce instanceof Habitacion) {
						System.out.println("Hola Barbitas");
						Habitacion h= (Habitacion) ce;
					}

					// Obtenemos los agente de conocimiento
					DFAgentDescription template_conocimiento = new DFAgentDescription();
					ServiceDescription sd = new ServiceDescription();
					sd.setType(tipo_conocimiento);
					template_conocimiento.addServices(sd);
					DFAgentDescription[] result = DFService.search(myAgent,
							template_conocimiento);

					if (result.length > 0) { // Si hay agentes de conocimiento
						int id_agente = rand.nextInt(result.length);
						AID agente_con = result[id_agente].getName();

						ACLMessage msg_conocimiento = new ACLMessage(
								ACLMessage.QUERY_REF);
						msg_conocimiento.addReceiver(agente_con);
						msg_conocimiento.setSender(myAgent.getAID());
						msg_conocimiento.setLanguage("Spanish");
						msg_conocimiento.setConversationId("decision");
						msg_conocimiento.setContent("Expresion solicitada");
						myAgent.send(msg_conocimiento);

						MessageTemplate template = MessageTemplate
								.and(MessageTemplate
										.MatchPerformative(ACLMessage.INFORM),
										MessageTemplate
												.MatchConversationId("decision"));

						// Anadimos un comportamiento que maneje el resultado
						// del agente de conocimiento
						myAgent.addBehaviour(new BehaviourReceiverMsg(myAgent,
								2000, template) {

							private static final long serialVersionUID = 1L;

							public void handle(ACLMessage msg) {
								try {
									if (msg != null) {
										// Mandamos la acci�n a realizar
										System.out
												.println("DEC --> Mensaje del agente de conocimiento");

										// Obtenemos los agente de acci�n
										DFAgentDescription template_accion = new DFAgentDescription();
										ServiceDescription sd = new ServiceDescription();
										sd.setType(tipo_accion);
										template_accion.addServices(sd);
										DFAgentDescription[] result = DFService
												.search(myAgent,
														template_accion);
										if (result.length > 0) { // Si hay
																	// agentes
																	// de
																	// conocimiento
											int id_agente = rand
													.nextInt(result.length);
											AID agente_con = result[id_agente]
													.getName();

											ACLMessage msg_accion = new ACLMessage(
													ACLMessage.REQUEST);
											msg_accion.addReceiver(agente_con);
											msg_accion.setSender(myAgent
													.getAID());
											msg_accion.setLanguage("Spanish");
											msg_accion
													.setConversationId("accion");
											msg_accion
													.setContent("Realizar accion");
											myAgent.send(msg_accion);

											MessageTemplate template = MessageTemplate
													.MatchConversationId("accion");

											// Anadimos un comportamiento que
											// maneje
											// el resultado del agente de
											// accion
											myAgent.addBehaviour(new BehaviourReceiverMsg(
													myAgent, 2000, template) {

												private static final long serialVersionUID = 1L;

												public void handle(
														ACLMessage msg) {
													if (msg != null) {
														if (msg.getPerformative() == ACLMessage.INFORM) {
															System.out
																	.println("DEC --> La acci�n se ha realizado correctamente");

														} else if (msg
																.getPerformative() == ACLMessage.FAILURE) {
															System.out
																	.println("DEC --> Ha habido alg�n fallo al realizar la acci�n");
														}
													}
												}
											});
										}

									}

								} catch (Exception er) {
									er.printStackTrace();
								}
							}
						});
					}

				} else {
					block();
				}
			} catch (FIPAException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UngroundedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CodecException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (OntologyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
