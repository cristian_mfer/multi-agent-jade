package isia.agents;

import isia.onto.Habitacion;
import isia.onto.Iluminacion;
import isia.onto.Ocupantes;
import isia.onto.OntologiaEdificioInteligente;
import isia.onto.Temperatura;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class SensorAgent extends Agent {
	private Ontology ontologia = OntologiaEdificioInteligente.getInstance();
	private Codec codec = new SLCodec();

	protected void setup() {
		try {
			long tiempo = 1000;
			String tipo;

			Object[] args = getArguments();

			// Parse JSON input file
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(new FileReader((String) args[0]));

			JSONObject jsonObject = (JSONObject) obj;

			// Obtenemos el periodo de muestreo del sensor
			tiempo = Long.parseLong((String) jsonObject.get("tiempo"));

			// Obtenemos el tipo del sensor
			tipo = (String) jsonObject.get("tipo");

			// Registramos el agente en las p�ginas amarillas
			DFAgentDescription dfd = new DFAgentDescription();
			dfd.setName(getAID());
			ServiceDescription sd = new ServiceDescription();
			sd.setType(tipo);
			sd.setName(getLocalName());
			dfd.addServices(sd);
			DFService.register(this, dfd);

			getContentManager().registerLanguage(codec);
			getContentManager().registerOntology(ontologia);
			addBehaviour(new SensorBehaviour(this, tiempo, tipo));

			System.out.println("SEN --> Agente sensor: " + getLocalName()
					+ " inicializado.");

		} catch (Exception er) {
			er.printStackTrace();
		}
	}

	protected void takeDown() {
		try {
			DFService.deregister(this);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		System.out.println("SEN --> El agente " + getAID().getName()
				+ " ya no ofrece sus servicios.");
	}

	private class SensorBehaviour extends TickerBehaviour {
		List agentes_decision;
		String tipo;
		Random rand;

		public SensorBehaviour(Agent a, long period, String tipo) {
			super(a, period);
			agentes_decision = new ArrayList();
			this.tipo = tipo;
			rand = new Random();
		}

		public int onEnd() {
			myAgent.doDelete();
			return super.onEnd();
		}

		protected void onTick() {
			MessageTemplate mt = MessageTemplate
					.MatchPerformative(ACLMessage.SUBSCRIBE);
			ACLMessage msg = myAgent.receive(mt);
			if (msg != null) {
				System.out.println("SEN --> Se ha recibido el mensaje: "
						+ msg.getContent());
				agentes_decision.add(msg.getSender());
			}
			String resultado = "";
			if (tipo.equals(AgentInterface.SENSOR_TEMP)) {
				Temperatura t = new Temperatura();
				t.setGrados(rand.nextInt(50));
				Iluminacion a=new Iluminacion();
				a.setLuminosidad(rand.nextInt(100));
				Ocupantes p = new Ocupantes();
				p.setNumero(rand.nextInt(500));
				Habitacion h = new Habitacion();
				h.setOcupantes(p);
				h.setTemperatura(t);
				h.setIluminacion(a);

				// int personas = rand.nextInt(500);
				// int temperatura = rand.nextInt(50);
				// resultado = personas + " " + temperatura;

				for (int i = 0; i < agentes_decision.size(); i++) {
					ACLMessage msg_send = new ACLMessage(ACLMessage.INFORM);
					msg_send.addReceiver((AID) agentes_decision.get(i));
					msg_send.setLanguage(codec.getName());
					msg_send.setOntology(ontologia.getName());
					msg_send.setSender(myAgent.getAID());
					// msg_send.setContent(resultado);
					try {
						getContentManager().fillContent(msg_send, h);
					} catch (CodecException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (OntologyException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					myAgent.send(msg_send);
				}
			}
		}
	}
}
