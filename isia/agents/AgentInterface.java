package isia.agents;

public interface AgentInterface {
	public static String SENSOR_TEMP="sensor_temperatura";
	public static String SENSOR_LUM="sensor_luminosidad";
	
	public static String ACCION_TEMP="accion_temperatura";
	public static String ACCION_LUM="accion_luminosidad";
	
	public static String CON_TEMP="conocimiento_temperatura";
	public static String CON_LUM="conocimiento_luminosidad";
	
}
