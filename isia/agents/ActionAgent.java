package isia.agents;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.io.FileReader;
import java.util.Random;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ActionAgent extends Agent {

	protected void setup() {
		try {
			String tipo;

			Object[] args = getArguments();

			// Parse JSON input file
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(new FileReader((String) args[0]));

			JSONObject jsonObject = (JSONObject) obj;

			// Obtenemos el tipo del agente de accion
			tipo = (String) jsonObject.get("tipo");

			// Registramos el agente en las p�ginas amarillas
			DFAgentDescription dfd = new DFAgentDescription();
			dfd.setName(getAID());
			ServiceDescription sd = new ServiceDescription();
			sd.setType(tipo);
			sd.setName(getLocalName());
			dfd.addServices(sd);
			DFService.register(this, dfd);

			addBehaviour(new ActionBehaviour());
			System.out.println("ACC -->Agente de accion " + getLocalName()
					+ " inicializado.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void takeDown() {
		try {
			DFService.deregister(this);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		System.out.println("ACC -->El agente " + getAID().getName()
				+ " ya no ofrece sus servicios.");
	}

	private class ActionBehaviour extends CyclicBehaviour {
		Random rnd = new Random();

		public void action() {
			MessageTemplate mt = MessageTemplate
					.MatchPerformative(ACLMessage.REQUEST);
			ACLMessage msg = myAgent.receive(mt);
			if (msg != null) {
				System.out.println("ACC --> Mensaje recibido: "
						+ msg.getContent());
				System.out
						.println("ACC --> El agente de acci�n est� realizando la acci�n deseada");
				ACLMessage respuesta = msg.createReply();

				if (rnd.nextInt(2) == 0) { // El agente de acci�n ha realizado
											// el trabajo correctamente
					respuesta.setPerformative(ACLMessage.INFORM);
					respuesta.setContent("Acci�n realizada correctamente");

				} else { // Simulamos la generaci�n de un error en el agente de
							// acci�n
					respuesta.setPerformative(ACLMessage.FAILURE);
					respuesta
							.setContent("Error al realizar la acccion solicitada");
				}
				send(respuesta);

			} else {
				block();
			}
		}

		public int onEnd() {
			myAgent.doDelete();
			return super.onEnd();
		}
	}
}
