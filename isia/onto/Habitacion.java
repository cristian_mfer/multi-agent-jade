package isia.onto;

import jade.content.Predicate;

public class Habitacion implements Predicate {
	private int superficie;
	private Iluminacion iluminacion;
	private Ocupantes ocupantes;
	private Temperatura temperatura;
	

	public int getSuperficie() {
		return superficie;
	}

	public void setSuperficie(int superficie) {
		this.superficie = superficie;
	}

	public Iluminacion getIluminacion() {
		return iluminacion;
	}

	public void setIluminacion(Iluminacion iluminacion) {
		this.iluminacion = iluminacion;
	}
	
	public Ocupantes getOcupantes() {
		return ocupantes;
	}

	public void setOcupantes(Ocupantes ocupantes) {
		this.ocupantes = ocupantes;
	}

	public Temperatura getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(Temperatura temperatura) {
		this.temperatura = temperatura;
	}
}
