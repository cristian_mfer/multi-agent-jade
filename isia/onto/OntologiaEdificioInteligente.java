package isia.onto;
import jade.content.Predicate;
import jade.content.onto.BasicOntology;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.schema.AgentActionSchema;
import jade.content.schema.ConceptSchema;
import jade.content.schema.ObjectSchema;
import jade.content.schema.PredicateSchema;
import jade.content.schema.PrimitiveSchema;

public class OntologiaEdificioInteligente extends Ontology {

	//Vocabulario
	public static final String OCUPANTES = "Ocupantes";
	public static final String TEMPERATURA = "Temperatura";
	public static final String ILUMINACION = "Iluminacion";
	public static final String HABITACION = "Habitacion";
	
	public static final String OCUPANTES_NUMERO = "Numero";
	public static final String TEMPERATURA_GRADOS = "Grados";
	public static final String ILUMINACION_LUMINOSIDAD = "Luminosidad";
	public static final String HABITACION_SUPERFICIE = "Superficie";
	public static final String HABITACION_ILUMINACION = "Iluminacion";
	public static final String HABITACION_TEMPERATURA = "Temperatura";
	public static final String HABITACION_OCUPANTES = "Ocupantes";
	
	private static final String MODIFICAR_TEMPERATURA = "ModificarTemperatura";
	private static final String MODIFICAR_TEMPERATURA_TEMPERATURA = "Temperatura";
	private static final String MODIFICAR_LUMINOSIDAD = "ModificarLuminosidad";
	private static final String MODIFICAR_TEMPERATURA_LUMINOSIDAD = "Luminosidad";

	private static Ontology instancia = new OntologiaEdificioInteligente();

	public static Ontology getInstance() {
		return instancia;
	}

	public OntologiaEdificioInteligente() {
		super("ontologia-edificio-inteligente", BasicOntology.getInstance());

		try {
			add(new ConceptSchema(OCUPANTES), Ocupantes.class);
			add(new ConceptSchema(TEMPERATURA), Temperatura.class);
			add(new ConceptSchema(ILUMINACION), Iluminacion.class);
			add(new PredicateSchema(HABITACION), Habitacion.class);
			
			add(new AgentActionSchema(MODIFICAR_TEMPERATURA), ModificarTemperatura.class);
			add(new AgentActionSchema(MODIFICAR_LUMINOSIDAD), ModificarLuminosidad.class);
			
			// Atributos de los conceptos
			ConceptSchema cs = (ConceptSchema) getSchema(OCUPANTES);
			cs.add(OCUPANTES_NUMERO, (PrimitiveSchema) getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY);
			
			cs = (ConceptSchema) getSchema(TEMPERATURA);
			cs.add(TEMPERATURA_GRADOS, (PrimitiveSchema) getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY);
			
			cs = (ConceptSchema) getSchema(ILUMINACION);
			cs.add(ILUMINACION_LUMINOSIDAD, (PrimitiveSchema) getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY);
			
			PredicateSchema ps = (PredicateSchema) getSchema(HABITACION);
			ps.add(HABITACION_SUPERFICIE, (PrimitiveSchema) getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY);
			ps.add(HABITACION_ILUMINACION, (ConceptSchema) getSchema(ILUMINACION), ObjectSchema.OPTIONAL);
			ps.add(HABITACION_TEMPERATURA, (ConceptSchema) getSchema(TEMPERATURA), ObjectSchema.OPTIONAL);
			ps.add(HABITACION_OCUPANTES, (ConceptSchema) getSchema(OCUPANTES), ObjectSchema.OPTIONAL);
				
			// Estructura de las acciones
			AgentActionSchema as = (AgentActionSchema) getSchema(MODIFICAR_TEMPERATURA);
			as.add(MODIFICAR_TEMPERATURA_TEMPERATURA, (ConceptSchema) getSchema(TEMPERATURA));
			
			AgentActionSchema as1 = (AgentActionSchema) getSchema(MODIFICAR_LUMINOSIDAD);
			as1.add(MODIFICAR_TEMPERATURA_LUMINOSIDAD, (ConceptSchema) getSchema(ILUMINACION));


		} catch (OntologyException oe) { oe.printStackTrace();}
	}

}
