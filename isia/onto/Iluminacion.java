package isia.onto;

public class Iluminacion {
	private int luminosidad;

	public int getLuminosidad() {
		return luminosidad;
	}

	public void setLuminosidad(int luminosidad) {
		this.luminosidad = luminosidad;
	}
}
