package isia.behaviours;

import isia.agents.SensorAgent;
import jade.core.Agent;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

public class ExecAgents {
	public static void main(String[] args) throws StaleProxyException
	{	
		jade.core.Runtime runtime = jade.core.Runtime.instance();
		Profile profile = new ProfileImpl();
		
		// profile.setParameter( ... );
		AgentContainer container = runtime.createMainContainer( profile );
		Agent agent = new SensorAgent();
		
		// agent.addBehaviour( ... );
		Object [] input=new Object[1];
		input[0]="";
		AgentController ac = container.acceptNewAgent( "julio", agent);
//		container.("input-julio", agent,input)
		ac.start();
		
	}

}
