# README #

Programación de  un sistema multiagente basado en modelos BDI.
==========
 
## Elementos ##

**Paquete isia.agents:** con el conjunto de agentes del sistema.
*  SensorAgent.java (Agente sensor)
*  DecisionAgent.java (Agente de decisión)
*  DatabaseAgent.java (Agente de conocimiento)
*  ActionAgent.java (Agente de acción)

**Paquete isia.behaviours:** con comportamientos auxiliares:
*  BehaviourReceiverMsg.java (Comportamiento con tiempo de espera)

**Paquete isia.json:** con ficheros json de configuración de los agentes.

## Ejecución ##
Se debe de ejecutar la plataforma Jade en primera lugar. Una vez iniciada la plataforma, se debe de iniciar cada agente en la misma con su fichero de configuración correspondiente, debiéndose ejecutar en última posición el agente de decisión.

## Contribuciones ##
* Cristian Martín Fernández.
* Julio Montes Torres.